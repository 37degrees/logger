Package.describe({
  name:          '37degrees:logger',
  version:       '0.0.1',
  summary:       'Logger & Exception Handler wrapped around TraceX',
  git:           'https://gitlab.com/37degrees/logger',
  documentation: 'README.md'
});

Package.onUse(function (api) {
  api.versionsFrom('1.2.1');

  api.use([
    'ecmascript',
    'meteor-platform',
    'reactive-var',
    'underscore',
    'arsnebula:classx@2.0.5',
    'arsnebula:reactive-varx@0.9.2',
    'arsnebula:tracex@0.9.0',
    'peerlibrary:stacktrace@0.2.0'
  ]);

  api.addFiles([
    'lib/cycle.js',
    'lib/logger.namespace.js',
    'lib/logger.service.js',
    'lib/logger.log.js'
  ], ['client', 'server']);

  api.export('d37logger', ['server', 'client']);

  api.export('d37logit', ['server', 'client']);
});
