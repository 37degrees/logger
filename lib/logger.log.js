d37logger.Logger = ClassX.extend(d37logger.Class, function (base) {

  var ctx = this;

  var isValidJson = function (json) {
    try {
      JSON.parse(json);
    }
    catch (e) {
      //consoleLog("isValidJson exception", e);
      return false;
    }
    return true;
  };

  var raiseEvent = function (level, message, meta, skipStackTrace) {
    var metaData, logMsg;

    // Handle SimpleSchema errors triggered by collection operations - they aren't valid JSON
    if (meta && meta.error && typeof meta.error !== "string" && !isValidJson(meta)) {
      try {
        if (meta.error["sanitizedError"]) {
          //console.log("raiseEvent meta.sanitizedError:", meta.error.sanitizedError);

          meta = {
            error:   meta.error.sanitizedError.error,
            reason:  meta.error.sanitizedError.reason
          };

          //TODO: This seemed to work, but isn't now... investigate later
          //try {
          //  meta.details = JSON.parse(meta.error.sanitizedError.details);
          //}
          //catch (e) {
          //  console.log("meta.error.sanitizedError.details", meta.error.sanitizedError.details);
          //  consoleLog("JSON.parse(meta.error['sanitizedError'].details) exception", e);
          //}
        } else {
          meta = {}
        }
      }
      catch (err) {
        consoleLog('raiseEvent meta.sanitizedError check exception', err);
      }
    }

    try {
      metaData = getMeta(meta, isFullStackTrace(level), skipStackTrace);
      message  = getMessage(message);

      // first trigger any additional event listeners, if configured
      if (ctx._ns.settings.listeners && ctx._ns.settings.listeners.length > 0) {
        _.each(ctx._ns.settings.listeners, function (id) {
          logMsg = ('[d37logger.Logger] raiseEvent. ID/Level/Message: {0}/{1}/{2}')
            .format(id, level, message);
          //ctx._ns.settings.debug && console.log(logMsg);
          ctx._ns.raiseEvent(id, { 'level': level, 'msg': message, 'meta': metaData }, true);
        });
      }

      // Then trigger primary event (exception could kill server so trigger other listeners first)
      ctx._ns.raiseEvent('tracex', { 'level': level, 'msg': message, 'meta': metaData }, true);
    }
    catch (err) {
      consoleLog('raiseEvent exception', err);
    }
  };

  var isFullStackTrace = function (level) {
    if (ctx._ns.settings.stackTrace.enabled && ctx._ns.settings.stackTrace.fullStackLevels) {
      return ctx._ns.settings.stackTrace.fullStackLevels.indexOf(level) > -1;
    }
    return false;
  };

  var getMetaWithoutTrace = function (meta) {
    return getMeta(meta, null, true);
  };

  var getMeta = function (meta, isFullStackTrace, skipStackTrace) {
    var metaData = {};

    try {
      metaData = _.extend({}, meta);
    }
    catch (err) {
      consoleLog('getMeta exception', err);

      if (meta && typeof meta === 'object') {
        try {
          metaData = JSON.decycle(meta);
        }
        catch (err) {
          consoleLog('getMeta JSON.decycle exception', err);
        }
      }
    }

    try {
      // Get stack trace
      if (ctx._ns.settings.stackTrace.enabled && !skipStackTrace) {
        metaData.stackTrace = isFullStackTrace ? getFullStackTrace() : getStackTrace();
      }

      // Get client user ID
      if (Meteor.isClient) {
        metaData.userId = Meteor.userId();
      }
    }
    catch (err) {
      consoleLog('getMeta router or stacktrace exception', err);
    }

    return metaData;
  };

  var getMessage = function (message) {
    if (typeof message === 'undefined' || !message || message === '') {
      message = '[no message]';
    }
    return message;
  };

  var getFullStackTrace = function () {
    try {
      return getFilteredStackTrace();
    }
    catch (err) {
      consoleLog('getFullStackTrace StackTrace.printStackTrace exception', err);
    }
  };

  var getStackTrace = function () {
    try {
      if (StackTrace && StackTrace.printStackTrace) {
        var array = StackTrace.printStackTrace();
        if (array && array.constructor === Array) {
          //console.log('FB_Utils.Logger getStackTrace array length: ' + array.length);
          //console.log(StackTrace.printStackTrace());
          for (var i = 0; i < array.length; i++) {
            //console.log(array[i]);
            //console.log(JSON.stringify(array[i]));
            if (isReleventStackTraceItem(array[i])) {
              return array[i];
            }
          }
        }
      }
    }
    catch (err) {
      consoleLog('getFullStackTrace StackTrace.getStackTrace exception', err);
    }
  };

  var getFilteredStackTrace = function () {
    try {
      var trace = [],
          array = StackTrace.printStackTrace();

      if (array && array.constructor === Array) {
        //console.log('FB_Utils.Logger getStackTrace array length: ' + array.length);
        //console.log(StackTrace.printStackTrace());
        for (var i = 0; i < array.length; i++) {
          //console.log(array[i]);
          //console.log(JSON.stringify(array[i]));
          if (isReleventStackTraceItem(array[i])) {
            trace.push(array[i]);
          }
        }
      }

      return trace;
    }
    catch (err) {
      consoleLog('getFilteredStackTrace StackTrace.getStackTrace exception', err);
    }
  };

  var isReleventStackTraceItem = function (item) {
    return item.indexOf('peerlibrary:stacktrace') < 0
      && item.indexOf('peerlibrary_stacktrace') < 0
      && item.indexOf('arsnebula:tracex') < 0
      && item.indexOf('flybuy_logger') < 0
      && item.indexOf('flybuy:logger') < 0;
  };

  var consoleLog = function (message, error) {
    var msg = '[d37logger.Logger] ' + message;
    if (error) {
      if (error && typeof error === 'object') {
        error = JSON.decycle(error);
      }
      msg += ' ' + JSON.stringify(error);
    }
    console.log(msg);
  };

  this.log             = function (level, message, meta) {
    Tracker.nonreactive(function () {
      raiseEvent(level, message, meta);
    });
  };
  this.debug           = function (message, meta) {
    Tracker.nonreactive(function () {
      raiseEvent('debug', message, meta);
    });
  };
  this.info            = function (message, meta) {
    Tracker.nonreactive(function () {
      raiseEvent('info', message, meta);
    });
  };
  this.warning         = function (message, meta) {
    Tracker.nonreactive(function () {
      raiseEvent('warning', message, meta);
    });
  };
  this.exception       = function (message, meta) {
    Tracker.nonreactive(function () {
      raiseEvent('exception', message, meta);
    });
  };
  this.error           = function (message, meta) {
    Tracker.nonreactive(function () {
      raiseEvent('error', message, meta);
    });
  };
  this.job             = function (name, message, meta, level) {
    Tracker.nonreactive(function () {
      meta      = meta || {};
      meta.type = 'job';
      meta.name = name;
      level     = level || 'info';
      raiseEvent(level, message, meta);
    });
  };
  this.clientException = function (message, url, lineNbr, column, errorObj, meta) {
    var metaData          = getMetaWithoutTrace(meta);
    metaData.layer        = 'client';
    metaData.category     = 'exception';
    metaData.errorMessage = message;
    if (errorObj && errorObj.stack) {
      metaData.errorStack = errorObj.stack;
    }
    metaData.url        = url;
    metaData.lineNumber = lineNbr;
    metaData.column     = column;
    Tracker.nonreactive(function () {
      raiseEvent('exception', 'An uncaught exception was thrown by the client app.', metaData);
    });
  };
  this.serverException = function (err, meta) {
    var metaData;

    // log first in case anything goes wrong with
    console.log("[d37logger.serverException]", err, meta);

    metaData              = getMetaWithoutTrace(meta);
    metaData.layer        = 'server';
    metaData.category     = 'exception';
    metaData.errorMessage = err && err.message ? err.message : null;
    metaData.errorStack   = err && err.stack ? err.stack : null;

    Tracker.nonreactive(function () {
      raiseEvent('exception', 'An uncaught exception was thrown by the server.', metaData);
    });
  };

  // Custom loggers by category
  this.routeError = function (message, meta) {
    this.route(message, meta, 'error');
  };

  this.route        = function (message, meta, level) {
    var userId     = Meteor.userId() || 'UNKNOWN',
        level      = level || 'debug',
        name       = getCurrentRouteName(),
        path       = getCurrentRoutePath(),
        nameOrPath = (name || path);

    meta = getMeta(meta, isFullStackTrace(level), true);

    meta.category = 'router';
    meta.name     = name;
    meta.path     = path;
    meta.userId   = userId;

    message = "Route '{0}' hit by user ID {1}. {2}".format(nameOrPath, userId, message);
    Tracker.nonreactive(function () {
      raiseEvent(level, message, meta);
    });
  };
  this.template     = function (template, event, message) {
    var level        = 'debug',
        meta         = getMeta(meta, isFullStackTrace(level));
    meta.category    = 'template';
    meta.subcategory = event;
    meta.template    = template;
    Tracker.nonreactive(function () {
      raiseEvent(level, message, meta);
    });
  };
  this.clientAction = function (event, context, template) {
    var level = 'debug',
        message,
        meta  = getMeta({}, isFullStackTrace(level));

    meta.category    = 'template';
    meta.subcategory = event;

    //TODO: message doesn't seem to be set properly here... what was intention?  BT: Is it working now...?
    if (template) {
      meta.template = template;
      meta.context  = context;
      message       = '[Template.{0}] {1} {2}'.format(template, context, event);
    } else {
      message = '{0} {1} {2}'.format('Template Event::', context, event);
    }

    Tracker.nonreactive(function () {
      raiseEvent(level, message, meta);
    });
  };

  this.constructor = function (meta) {
    //console.log("[d37logger.Logger] constructor initiated.", meta);
    base.constructor.call(this);
    this.settings = {
      "meta": meta
    };
  };
});
