d37logger.LoggerService = ClassX.extend(d37logger.Class, function (base) {

    // auto-log global events
    var onLogEvent = function () {
        var ctx = this;

        return function (data) {
            var msg;

            // validate that the data object has accepted properties
            var valid = Match.test(data, {
                'level': String,
                'msg':   Match.OneOf(String, Object),
                'meta':  Match.Optional(Object)
            });

            var logger = new ctx._ns.Logger();

            if (valid) {
                // use trace logger to send message to our self
                // to ensure proper call formatting and rules
                data.meta = data.meta || {};
                logger.log(data.level, data.msg, data.meta);

            } else {
                msg = '[d37logger.LoggerService.onLogEvent] invalid parameters passed: '
                + JSON.stringify(data);
                logger.log('error', msg);
            }
        };
    };

    this.constructor = function LoggerService () {
        //console.log('[d37logger.LoggerService] constructor initiated.');

        base.constructor.call(this);

        // register global event listener
        var eventId = this._ns.settings.event.id;
        this.addEventListener(eventId, onLogEvent.call(this), true);
    };

});
