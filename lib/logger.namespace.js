d37logger = new (ClassX.extend(ClassX.Class, function (base) {

    var getDefaultSettings = function () {
        return {
            debug:                  false,
            event:                  {
                id: 'd37logger'     // the id of the event to listen and auto-log
            },
            // listeners: [],       // additional listeners to trigger on events
            tracex:                 {
                event:         {
                    id: 'tracex'    // the id of the event to listen and auto-log
                },
                console:       {
                    // format for console logging (text, meta or json)
                    format: 'meta',
                    levels: {
                        // array of log levels to log to the client or server console (or null)
                        client: ['debug', 'info', 'warning', 'error', 'exception'],
                        server: ['debug', 'info', 'warning', 'error', 'exception']
                    }
                },
                database:      {
                    collection: 'eventlog', // name of the database collection --- tracex is the default
                    // array of log levels to log to the database (or null)
                    levels:     ['debug', 'info', 'warning', 'error', 'exception']
                }
            },
            // whether to enable capturing stack trace, and what types
            stackTrace:             {
                enabled:         false,
                fullStackLevels: ['exception', 'error']
            },
            globalExceptionHandler: {
                handleClient: true,
                handleServer: true
            }
        }
    };

    var clientGlobalExceptionHandler = function () {
        // Handle all uncaught client exceptions
        if (Meteor.isClient) {
            window.onerror = function (message, url, lineNbr, column, errorObj) {
                var logger;

                console.error("[d37logger.clientGlobalExceptionHandler] window.onerror:",
                    message, url, lineNbr, column, errorObj);

                logger = new d37logger.Logger();
                logger.clientException(message, url, lineNbr, column, errorObj)
            }
        }
    };

    var serverGlobalExceptionHandler = function () {
        if (Meteor.isServer) {
            // Handle all uncaught server exceptions
            process.on('uncaughtException', function (err) {
                var Fiber,
                    logger;

                console.error("[d37logger.serverGlobalExceptionHandler] uncaughtException:", err);

                Fiber = Npm.require('fibers');
                Fiber(function () {
                    logger = new d37logger.Logger();
                    logger.serverException(err);
                }).run();
                process.exit(1);
            });
        }
    };

    this.init = function (options) {
        // the namespace can only be initialized once
        if (this.__init) {
            return false;
        }

        // merge options with default settings
        this.__settings.extend({}, options);

        options.debug && console.log("[d37logger] init options: " + JSON.stringify(options));
        this.settings.debug && console.log("[d37logger] init settings: " + JSON.stringify(this.settings));

        // Init TraceX so it can pickup its events
        TraceX.init(this.settings.tracex);

        this.logger  = new d37logger.Logger();
        this.service = new d37logger.LoggerService();

        // Setup global exception handler for server
        if (this.settings.globalExceptionHandler.handleClient) {
            clientGlobalExceptionHandler();
        }
        if (this.settings.globalExceptionHandler.handleServer) {
            serverGlobalExceptionHandler();
        }

        this.__init = true;
        return this.__init;
    };

    // provide read-access to settings
    Object.defineProperty(this, 'settings', {
        'get': function () {
            return this.__settings.get();
        }
    });

    this.constructor = function d37logger () {
        //console.log("[d37logger] constructor initiated.");
        base.constructor.call(this);
        this.__settings = new ReactiveVar(getDefaultSettings.call(this));
        this.__init     = false;
    };
}))
();

d37logger.Class = ClassX.extend(ClassX.Class, function (base) {
    Object.defineProperty(this, "_ns", {
        "get": function () {
            return d37logger;
        }
    });
});
